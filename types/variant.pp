# @summary Contents of a variants file
#
# The file will be in /etc/ganeti/instance-debootstrap/variants/$name
#
# The values given to `extra_variables` will be exported so that they become
# available to the hook scripts. This can let you define any arbitrary
# customization for how you install the OS.
#
# @see http://docs.ganeti.org/ganeti/master/html/man-ganeti-os-interface.html
#
type Ganeti::Variant = Struct[{
  Optional[suite]           => String[1],
  Optional[components]      => String[1],
  Optional[mirror]          => String[1],
  Optional[target]          => String[1],
  Optional[arch]            => String[1],
  Optional[partition_style] => String[1],
  Optional[extra_pkgs]      => String[1],
  Optional[blockdev]        => String[1],
  Optional[fsysdev]         => String[1],
  Optional[extra_variables] => Hash[String[1],String[1],1],
  Optional[extra_code]      => String[1],
}]
