require 'socket'

gnt_vardir = '/var/lib/ganeti'

# This fact is deprecated and will be removed in a future release. Please use
# the structured 'ganeti' fact instead.
# This fact corresponds to $facts['ganeti']['cluster_name']
Facter.add("ganeti_cluster") do
  confine :kernel => :linux

  setcode do
    if File.exists?("#{gnt_vardir}/ssconf_cluster_name") then
      %x{cat #{gnt_vardir}/ssconf_cluster_name}.chomp
    end
  end
end

# This fact is deprecated and will be removed in a future release. Please use
# the structured 'ganeti' fact instead.
# This fact corresponds to $facts['ganeti']['cluster_version']
Facter.add("ganeti_cluster_version") do
  confine :kernel => :linux

  setcode do
    if File.exists?("#{gnt_vardir}/ssconf_release_version") then
      %x{cat #{gnt_vardir}/ssconf_release_version}.chomp
    end
  end
end

# This fact is deprecated and will be removed in a future release. Please use
# the structured 'ganeti' fact instead.
# This fact corresponds to $facts['ganeti']['is_master']
Facter.add("is_ganeti_master") do
  confine :kernel => :linux

  setcode do
    if File.exists?("#{gnt_vardir}/ssconf_master_node") then
      cmdline = %x{cat #{gnt_vardir}/ssconf_master_node}.chomp
      hostname = Addrinfo.getaddrinfo(Socket.gethostname, nil).first.getnameinfo.first
      hostname == cmdline
    else
      false
    end
  end
end

def get_IPs_for_nodes(nodes, file_name, key_name)
  if File.exists?(file_name) then
    primary_ips = {}
    %x{cat #{file_name}}.chomp.split(/\n/).each do |line|
      l = line.split(/ /)
      primary_ips[l[0]] = l[1]
    end

    nodes.each do |k,v|
      if primary_ips.has_key? k then
        nodes[k][key_name] = primary_ips[k]
      else
        nodes[k][key_name] = nil
      end
    end
  else
    # We have no info in file_name
    nodes.each do |k,v|
      nodes[k][key_name] = nil
    end
  end
end

def get_nodes_in_file(nodes, file_name, key_name)
  if File.exists?(file_name) then
    offline_nodes = %x{cat #{file_name}}.chomp.split(/\n/)
    nodes.each do |k,v|
      if offline_nodes.include? k then
        nodes[k][key_name] = true
      else
        nodes[k][key_name] = false
      end
    end
  else
    # No information, assume that all nodes are online
    nodes.each do |k,v|
      nodes[k][key_name] = false
    end
  end
end

Facter.add(:ganeti) do
  setcode do
    ganeti_cluster_name = Facter.value(:ganeti_cluster)
    ganeti_cluster_version = Facter.value(:ganeti_cluster_version)
    is_ganeti_master = Facter.value(:is_ganeti_master)

    # TODO : expose master node's ssh public key

    # Information about nodes
    if File.exists?("#{gnt_vardir}/ssconf_node_list") then
      nodes = {}
      %x{cat #{gnt_vardir}/ssconf_node_list}.chomp.split(/\n/).each do |node|
        nodes[node] = {}
      end

      # Get info about node primary IPs
      get_IPs_for_nodes(
        nodes, "#{gnt_vardir}/ssconf_node_primary_ips", 'primary_ip')
      # Now the same exact thing but for secondary IPs
      get_IPs_for_nodes(
        nodes, "#{gnt_vardir}/ssconf_node_secondary_ips", 'secondary_ip')

      # Now get the list of offline nodes
      get_nodes_in_file(
        nodes, "#{gnt_vardir}/ssconf_offline_nodes", 'offline')

      # Identify which nodes are master candidates
      get_nodes_in_file(
        nodes, "#{gnt_vardir}/ssconf_master_candidates", 'master_candidate')

    else
      # No list of nodes present
      nodes = {}
    end

    if File.exists?("#{gnt_vardir}/ssconf_instance_list") then
      instances = %x{cat #{gnt_vardir}/ssconf_instance_list}.chomp.split(/\n/)
    else
      instances = []
    end

    {
      "cluster_name"    => ganeti_cluster_name,
      "cluster_version" => ganeti_cluster_version,
      "is_master"       => is_ganeti_master,
      "nodes"           => nodes,
      "instances"       => instances,
    }
  end
end

