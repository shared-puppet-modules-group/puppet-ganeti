Ganeti Puppet support
=====================

Description
-----------

The `ganeti` module installs and configures packages to configure a
[Ganeti][] cluster.

[Ganeti]: http://www.ganeti.org/

Setup
-----

This module does not initialize a cluster or add nodes, which are
expected to be pre-installed in the cluster. However, it will properly
prepare nodes with the right software to do so. So normally, you
should only have to run `gnt-cluster init` on the master and `gnt-node
add` for each secondary node.

Requires the [`camptocamp/kmod`][] module in order to load the right
kernel modules when the module is configured `with_drbd`.

[`camptocamp/kmod`]: https://github.com/anarcat/puppet-kmod

Usage
-----

The main `ganeti` class handles most of the work:

```
include ganeti
```

It sets up ganeti packages and also deploys a few set of custom hooks
to make `gnt-instance-debootstrap` work properly. Extra hooks can be
deployed manually in `/etc/ganeti/instance-debootstrap/hooks/` as
needed. The debootstrap configuration can be modified through the
`bootstrap_defaults` parameter. This, for example, will add extra
pacakges to every install:

```
  class { ganeti :
    debootstrap_defaults => {
      extra_packages => ['linux-image-amd64', 'dbus', 'libpam-systemd'],
    }
  }
```

Note that, by default, `gnt-instance-deboostrap` does *not* install a
kernel, so the above `extra_packages` is actually likely to be needed
to get a working install on Debian virtual machines. This will vary
based on the guest operating system; on Ubuntu, it might instead be
the `linux-image-generic` package, for example.

### Creating a swap partition at instance creation

If you are using `ganeti-instance-debootstrap` to provision the base install
for your instances, this module provides a hook that will help you setup a
disk as a swap partition upon instance creation.

To achieve this, you need to label (name) one of the disk as "swap", as in the
following example:

```
gnt-instance add -d -o debootstrap+bullseye -t plain \
 --disk 0:size=14G --disk 1:size=1G,name=swap
 instance1.example.com
```

Here, we have one disk of 1Gb that's named "swap": this will be used as swap
space automatically. The other disk will be used as the main disk to install
the system on.

Reference
---------

See [REFERENCE.md][] for full reference documentation. Note that this file is
generally only generated when a tag is added.

The full online reference documentation for this module may be found at on
[GitLab Pages][pages]. This online reference is generated automatically on
each new commit.

Alternatively, you may build yourself the documentation using the
`puppet strings generate` command. See the documentation for
[Puppet Strings][strings] for more information.

[pages]: https://shared-puppet-modules-group.gitlab.io/ganeti
[strings]: https://puppet.com/blog/using-puppet-strings-generate-great-documentation-puppet-modules

Limitations
-----------

Tested on Debian stretch, buster, and bullseye. Compatibility with
other versions and distributions unknown.

Development
-----------

Developed and maintained by the shared puppet modules group.
