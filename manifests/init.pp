# @summary Configure a drbd node server
#
# @param with_drbd
#   Set this to false to disable installing drbd on the node.
# @param cron_template
#   Path to the template used for configuring cronjobs for ganeti. The
#   template is expected to be an epp template.
# @param security_model
#   If set to 'pool', will create a number of non-privileged users that ganeti
#   will use to run instances with. This is mostly useful when using the kvm
#   backend.
# @param pool_uid_base
#   When security_model is set to 'pool', users that get created will start
#   with this uid and all additional ones will have uids incremented from this
#   number.
# @param pool_num_users
#   When security_model is set to 'pool', number of users with sequential uids
#   that will get created for the pool.
# @param pool_username_prefix
#   When security_model is set to 'pool', users that get created will have a
#   username that starts with this string. Names will then be completed with
#   the increment on the uid_base, starting at 0, appended to the prefix.
# @param private_ip
#   IP address configured on the node that is not facing a public network. It
#   is strongly recommended to configure nodes to have a second interface that
#   is part of a network that's not reachable through a public network. This
#   makes it possible to hide away ganeti commands, drbd replication traffic
#   and other sensitive information. This module will also configure ganeti
#   services to listen only to the private IP, if provided. See this post for
#   more information on how that can be configured:
#   https://groups.google.com/forum/#!searchin/ganeti/secondary$20ip|sort:date/ganeti/XBRWU7LwA6g/QnxpAo5HAQAJ
# @param rapi_bind
#   Set the address for the Ganeti remote API daemon to bind to. By default this
#   is 127.0.0.1 but 0.0.0.0 may be used to bind to all interfaces, including
#   localhost, which is the ganeti-rapi daemon default.
# @param rapi_ip
#   Set the address to use to access the Ganeti remote API daemon.
# @param rapi_users
#   Define credentials for the Ganeti remote API daemon, and used to populate
#   the file /var/lib/ganeti/rapi/users. Must consist of an array of hashes
#   containing username, password and options keys. Default is not to define any
#   credentials (empty).
# @param use_debootstrap_os_template
#   Set this to false to avoid installing and configuring
#   ganeti-instance-debootstrap, the OS template that uses debootstrap. All
#   params `purge_instance_debootstrap` and `debootstrap_*` will have no
#   effect if this option is set to false. Users must make sure to keep os
#   template parameters consistent across all nodes in the same cluster.
# @param purge_instance_debootstrap
#   By default, this module removes all unknown configuration in the
#   ganeti-instance-debootstrap package creation template so that
#   instances created are using a known configuration. Set this value
#   to false in order to disable purging of the hooks directory.
# @param package_ensure
#   Set the ensure of the package. Useful to have a specific version
#   installed.
# @param debootstrap_defaults
#   Set of configuration options that define the default behaviour for
#   instances created with ganeti-instance-debootstrap. The values in this hash
#   correspond to values in /etc/default/ganeti-instance-debootstrap. Refer to
#   `types/debootstrap_config.pp` to see all of the available options and their
#   expected data types.
# @option debootstrap_defaults [String[1]] :proxy
#   If set, configure apt to use a proxy when downloading packages
# @option debootstrap_defaults [String[1]] :mirror
#   URL of the debian apt source used to retrieve packages
# @option debootstrap_defaults :[String[1]] arch
#   If set, will install instances with a different architecture than the host
# @option debootstrap_defaults :[String[1]] suite
#   If set, define the default suite used by debootstrap
# @option debootstrap_defaults [Array[String[1]]] :extra_packages
#   List of packages to have debootstrap install on top of the requested suite
# @option debootstrap_defaults :[Array[String[1]]] components
#   If set, define the suite components used by debootstrap (e.g.
#   "main,contrib,non-free")
# @option debootstrap_defaults [Stdlib::Absolutepath] :customize_dir
#   Absolute path to a directory containing hooks run after debootstrap is done
# @option debootstrap_defaults [Enum['yes', 'no']] :generate_cache: set to 'no' to disable package cache for debootstrap
# @option debootstrap_defaults [Integer[1]] :clean_cache: number of days during which packages are kept in cache
# @option debootstrap_defaults [Enum['none', 'msdos']] :partition_style: set to 'none' to avoid creating a partition table on
# @option debootstrap_defaults [String[1]] :partition_alignment: alignment of the partitions in sectors
# @param debootstrap_variants
#   Hash of definitions for debootstrapt variants that are defined on the
#   cluster. Each value should be a struct with any of the required values set
#   (see the type alias Ganeti::Variant for a complete list of available
#   values). An empty list will create an empty configuration file, which
#   means that all default values will be used.
# @param debootstrap_puppet_hook
#   Set this to `true` to install puppet inside instances when they are
#   installed with instance-debootstrap. Make sure to also set
#   `debootstrap_puppet_server`.
# @param debootstrap_puppet_server
#   Hostname or IP address of the puppet server to which the instances should
#   connect to. This value is only used when debootstrap_puppet_hook is set to
#   `true`.
# @param debootstrap_locale
#   Name of the default locale that will be configured in the new instances
#   when they get created by the debootstrap template. If set to undef
#   (default), locale will be untouched and no hook script for setting the
#   locale will be installed. This should be something legal in the
#   `locales` package, for example en_US or en_US.UTF-8, as
#   (currently) defined in /var/lib/dpkg/info/locales.config.
# @param debootstrap_charmap
#   Name of the character map (should correspond to the name of a file in
#   /usr/share/i18n/charmaps) used by the default locale configured in new
#   instances. This value is unused if debootstrap_locale is set to undef.
#
class ganeti (
  Hash[String, Ganeti::Variant] $debootstrap_variants,
  String[1]                     $cron_template               = 'ganeti/ganeti_cron.epp',
  Boolean                       $with_drbd                   = true,
  Optional[Enum['pool']]        $security_model              = 'pool',
  Integer                       $pool_uid_base               = 4000,
  Integer                       $pool_num_users              = 20,
  String[1]                     $pool_username_prefix        = 'ganeti',
  Optional[Stdlib::IP::Address] $private_ip                  = undef,
  Optional[Stdlib::IP::Address] $rapi_bind                   = '127.0.0.1',
  Optional[Stdlib::IP::Address] $rapi_ip                     = '127.0.0.1',
  Array[Ganeti::Rapi_user]      $rapi_users                  = [],
  Boolean                       $use_debootstrap_os_template = true,
  Boolean                       $purge_instance_debootstrap  = true,
  String[1]                     $package_ensure              = 'installed',
  Ganeti::Debootstrap_config    $debootstrap_defaults        = {},
  Boolean                       $debootstrap_puppet_hook     = false,
  Optional[Stdlib::Host]        $debootstrap_puppet_server   = undef,
  Optional[String[1]]           $debootstrap_locale          = undef,
  String[1]                     $debootstrap_charmap         = 'UTF-8',
) {

  if $use_debootstrap_os_template {
    if $debootstrap_puppet_hook and $debootstrap_puppet_server =~ Undef {
      fail('debootstrap puppet hook needs $debootstrap_puppet_server to be defined.')
    }
  }

  $debootstrap_min_defaults = {
    mirror         => 'http://deb.debian.org/debian',
    generate_cache => 'yes',
    clean_cache    => 14,
    extra_packages => [],
  }

  $debootstrap_default_values = $debootstrap_min_defaults + $debootstrap_defaults
  contain ganeti::package
  contain ganeti::config
  contain ganeti::service

  Class['ganeti::package']
  -> Class['ganeti::config']
  ~> Class['ganeti::service']

}
