# @summary Install packages required for a ganeti node
#
# @api private
#
class ganeti::package {

  assert_private()

  ensure_packages([
    'bridge-utils',
    'iputils-arping',
    'socat',
    'kpartx',
    'qemu-system',
  ],
    { ensure  => installed }
  )

  ensure_packages(
    [
      'ganeti'
    ],
    { ensure  => $ganeti::package_ensure }
  )

  if $ganeti::use_debootstrap_os_template {
    # ipv6calc is needed for the hook gnt-debian-interfaces
    # passwdqc is needed for the hook random-root-password
    ensure_packages([
      'ganeti-instance-debootstrap',
      'ipv6calc',
      'passwdqc',
    ],
      { ensure => installed }
    )
  }

  if $ganeti::with_drbd {
    package { 'drbd-utils':
      ensure => installed,
    }
  }
}
