# ganeti::service
#
# @summary Make sure that the ganeti services are running.
#
# @api private
#
class ganeti::service {

  assert_private()

  service { 'ganeti':
    ensure => running,
  }

  systemd::timer { 'prune-ganeti-queue.timer':
    active          => true,
    enable          => true,
    timer_content   => @(EOT),
      [Unit]
      Description=Run deletion of old jobs from the ganeti queue

      [Timer]
      OnCalendar=daily

      [Install]
      WantedBy=timers.target
      | EOT
    service_content => @(EOT),
      [Unit]
      Description=Delete old jobs from the ganeti queue
      ConditionPathExists=/var/lib/ganeti/queue

      [Service]
      Type=oneshot
      ExecStart=find /var/lib/ganeti/queue -type f -name 'job-*' -mtime +28 -delete
      | EOT
    # the queue dir is created by the service
    require         => Service['ganeti'],
  }

}
